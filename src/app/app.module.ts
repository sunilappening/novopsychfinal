import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Http } from '@angular/http';
// import { InAppBrowser } from '@ionic-native/in-app-browser';
// import { DocumentViewer,DocumentViewerOptions } from '@ionic-native/document-viewer';
import { PdfViewerComponent } from 'ng2-pdf-viewer';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
//import { TagsInputModule } from 'ionic2-tags-input';
import {IonTagsInputModule} from "ionic-tags-input";
import { Storage, IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';

import { CardsPage } from '../pages/cards/cards';
import { ContentPage } from '../pages/content/content';
import { ItemDetailPage } from '../pages/item-detail/item-detail';
import { MainPage } from '../pages/main-page/main-page';
import { LoginPage } from '../pages/login/login';
import { MenuPage } from '../pages/menu/menu';
import { ClientTabPage } from '../pages/client-tab/client-tab';
import { AssessmentTabPage } from '../pages/assessment-tab/assessment-tab';
import { SettingsPage } from '../pages/settings/settings';
import { SignupPage } from '../pages/signup/signup';
import { TabsPage } from '../pages/tabs/tabs';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { WelcomePage } from '../pages/welcome/welcome';
import { SchedulePage } from '../pages/schedule/schedule';
import { EmailPage } from '../pages/email/email';
import { ClientPage } from '../pages/client/client';
import { ClientCreatePage } from '../pages/client-create/client-create';
import { SelectAssessmentPage } from '../pages/select-assessment/select-assessment';
import { AccountPage } from '../pages/account/account';
import { AdministerPage } from '../pages/administer/administer';
import { ReportPage } from '../pages/report/report';
import { AddNotePage } from '../pages/add-note/add-note';
import {ClientDetailsComponent} from '../components/client-details/client-details';
import {AssessmentDetailsComponent} from '../components/assessment-details/assessment-details';
import {UpgradePlanComponent} from '../components/upgrade-plan/upgrade-plan';

import { Api } from '../providers/api';
import { Items } from '../providers/items';
import { Settings } from '../providers/settings';
import { User } from '../providers/user';
import { Clients } from '../providers/clients';
import { Assessments } from '../providers/assessments';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    option1: true,
    option2: 'Ionitron J. Framework',
    option3: '3',
    option4: 'Hello'
  });
}

@NgModule({
  declarations: [
    MyApp,
    CardsPage,
    ContentPage,
    ClientCreatePage,
    SelectAssessmentPage,
    ItemDetailPage,
    MainPage,
    LoginPage,
    MenuPage,
    ClientTabPage,
    AssessmentTabPage,
    SettingsPage,
    SignupPage,
    TabsPage,
    TutorialPage,
    WelcomePage,
    SchedulePage,
    EmailPage,
    ClientPage,
    AccountPage,
    AdministerPage,
    ReportPage,
    AddNotePage,
    ClientDetailsComponent,
    AssessmentDetailsComponent,
    UpgradePlanComponent,
    PdfViewerComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    //TagsInputModule,
    IonTagsInputModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    }),
    IonicModule.forRoot(MyApp,{tabsPlacement: 'top'}),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CardsPage,
    ContentPage,
    ClientCreatePage,
    SelectAssessmentPage,
    ItemDetailPage,
    MainPage,
    LoginPage,
    MenuPage,
    ClientTabPage,
    AssessmentTabPage,
    SettingsPage,
    SignupPage,
    TabsPage,
    TutorialPage,
    WelcomePage,
    SchedulePage,
    EmailPage,
    ClientPage,
    AccountPage,
    AdministerPage,
    ReportPage,
    AddNotePage
  ],
  providers: [
    Api,
    Items,
    User,
    Clients,
    Assessments,
    SplashScreen,
    StatusBar,
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
