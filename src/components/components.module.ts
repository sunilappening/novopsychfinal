import { NgModule } from '@angular/core';
import { ClientDetailsComponent } from './client-details/client-details';
import { AssessmentDetailsComponent } from './assessment-details/assessment-details';
import { UpgradePlanComponent } from './upgrade-plan/upgrade-plan';
@NgModule({
	declarations: [ClientDetailsComponent,
    AssessmentDetailsComponent,
    UpgradePlanComponent],
	imports: [],
	exports: [ClientDetailsComponent,
    AssessmentDetailsComponent,
    UpgradePlanComponent]
})
export class ComponentsModule {}
