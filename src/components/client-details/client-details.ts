import { Component, Input } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { Client } from '../../models/client';
import { ClientPage } from '../../pages/client/client';
import { ClientCreatePage } from '../../pages/client-create/client-create';
import { AddNotePage } from '../../pages/add-note/add-note';
import { Items } from '../../providers/providers';
import { SimpleChanges } from '@angular/core';

/**
 * Generated class for the ClientDetailsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'client-details',
  templateUrl: 'client-details.html'
})
export class ClientDetailsComponent {
  
  @Input() client: Client;
  clientNotes: any[] ;

  constructor(public modalCtrl: ModalController,public items: Items) {
    
    //this.text = "No Client Selected";
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      if (propName === 'client') {
        let client = changes['client'].currentValue;
        if(typeof client === 'undefined'){
          //do nothing
        }else{
          this.items.getClientNotes(client.id).subscribe(res => {
            this.clientNotes = res.notes;
          },
            err => {
              console.log(err);
            });
        }
        
      } 
      
    }
    
  }

  administer() {
    let modal = this.modalCtrl.create(ClientPage,{administer: 'administer'});
    modal.present();
  }

  emailAssessment(){
    let modal = this.modalCtrl.create(ClientPage,{administer: 'email'});
    modal.present();
  }

  scheduleAssessment(){
    let modal = this.modalCtrl.create(ClientPage,{administer: 'schedule'});
    modal.present();
  }

  addNote(clientId){
    let modal = this.modalCtrl.create(AddNotePage,{clientId: clientId});
    modal.onDidDismiss(item => {
      if (item) {
        let addedNote = this.items.addNote({clientId: clientId, title:item.title, body:item.body});
        this.clientNotes.push(addedNote);
      }
    })
    modal.present();
  }

  noteSelected(item){
    let modal = this.modalCtrl.create(AddNotePage,{clientId: item});
    modal.onDidDismiss(item => {
      if (item) {
        let note = this.items.addNote(item);
      }
    })
    modal.present();
  }

  editClient(item){
    let addModal = this.modalCtrl.create(ClientCreatePage, {client: item});
    addModal.onDidDismiss(item => {
      if (item) {
        console.log('inside editClient before calling the api')
        this.items.addClient(item);
      }
    })
    addModal.present();
  }
    

}
