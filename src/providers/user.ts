import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Api } from './api';
import 'rxjs/add/operator/map';

export class Acc {
  name: string;
  username: string;
 
  constructor(name: string, email: string) {
    this.name = name;
    this.username = email;
  }
}
/**
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class User {
  currentUser: Acc;

  constructor(public http: Http, public api: Api, public storage: Storage) {
    //this.api.url = "https://novopsych.com/api"
  }

  /**
   * Send a GET request to our login endpoint with the data
   * the user entered on the form.
   */
  login(credentials) {
    return this.api.login('login', credentials);
    }

  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  signup(accountInfo: any) {
    let seq = this.api.register('users', accountInfo).share();
    seq
      .map(res => res.json())
      .subscribe(res => {
        // If the API returned a successful response, mark the user as logged in
        if (res.status == 'success') {
          this._loggedIn(res);
        }
      }, err => {
        console.error('ERROR', err);
      });

    return seq;
  }

  /**
   * Log the user out, which forgets the session
   */
  logout() {
    this.currentUser = null;
  }

  /**
   * Process a login/signup response to store user data
   */
  _loggedIn(resp) {
    this.currentUser = resp.user;
  }

  forgotPassword(email: string){
    return 'success';
  }

  /**
   * Send a GET request to our login endpoint with the data
   * the user entered on the form.
   */
  //news() {
    //console.log("inside news");
    //let userData = localStorage.getItem('userData');
    //return this.api.get('news', userData);
      //.map(resp => resp.json());
      
  //}
    
}

