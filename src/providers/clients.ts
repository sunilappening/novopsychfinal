import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Client } from '../models/client';
import { Api } from './api';

@Injectable()
export class Clients {
  clients: Client[];
  client: Client;

  defaultItem: any = {
      "id": "84748",
      "user_id": "6324",
      "first_name": "Generic",
      "last_name": "Client",
      "sex_id": "3",
      "date_of_birth": "1800-01-01",
      "active": "1",
  };


  constructor(public http: Http, public api: Api) {
      this.api.getClients('clients').subscribe(res => {
          this.clients = res;
      },
          err => {
              console.log(err);
          });


  }

  getClients(params?: any) {
      if (!params) {
          return this.clients;
      }
      //return this.clients.map(client => client.filter((item) => {
      return this.clients.filter((item) => {
              return (item.first_name.toLowerCase().indexOf(params.toLowerCase()) > -1);
       })
        
  }

  addClient(client: Client) {
      let seq =  this.api.addClient('clients', client).share();
        seq
          .map(res => res.json().data.client)
          .subscribe(res => {
              console.log(res);
            // If the API returned a successful response, push the response in the clientsList
            this.clients.push(res);
            console.log('added the client to the list below')
            console.log(this.clients);
            
          }, err => {
            console.error('ERROR', err);
          });

        return seq;
    
  }

  deleteClient(item: Client) {
    this.clients.splice(this.clients.indexOf(item), 1);
  }

  addNote(note){
    let seq =  this.api.addNote('clients', note).share();
    seq
      .map(res => res.json().data.note)
      .subscribe(res => {
      }, err => {
        console.error('ERROR', err);
      });

    return seq;

  }

//   getClient(item){
//     console.log("inside getClient method of client provider...")
//     return this.api.getClientDetails('clients', item);
//     this.api.getClientDetails('clients', item).subscribe(res => {
//         this.client = res;
//     },
//         err => {
//             console.log(err);
//         });
//   }
}
