import { Injectable } from '@angular/core';

import { Http } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Api } from './api';
import { Clients } from './clients';
import { Assessments } from './assessments';

import { Item } from '../models/item';
import { Client } from '../models/client';


@Injectable()
export class Items {
  items: Client[];

  constructor(public http: Http, public api: Api, public clients: Clients, public assessments: Assessments) {
    
  }

  getNews(params?: any) {
    return this.api.getNews('notifications', params);

  }

  getClients(params?: any) {
    return this.clients.getClients(params);
  }

  getClientNotes(clientId: any) {
    return this.api.getClientNotes('clients', clientId);
  }

  getAssessments(params?: any) {
    return this.assessments.getAssessments(params);
  }

  query(params?: any) {
    return this.api.getNews('news', params);

  }

  addClient(client: Client) {
    return this.clients.addClient(client);
  }

  delete(item: Item) {
  }

  addNote(item) {
    return this.clients.addNote(item);
  }

}
