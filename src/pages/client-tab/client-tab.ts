import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { ClientCreatePage } from '../client-create/client-create';

import { Items } from '../../providers/providers';


import { Client } from '../../models/client';


/**
 * Generated class for the ClientTabPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-client-tab',
  templateUrl: 'client-tab.html',
})
export class ClientTabPage {

  currentItems: Client[];
  responseData: any;
  administer: string;
  client: Client;

  iters = new Array(10);

  constructor(public navCtrl: NavController, public navParams: NavParams, 
  public modalCtrl: ModalController, public items: Items, public viewCtrl: ViewController) {
}

  ionViewDidLoad() {
  //ionViewDidEnter(){
    this.currentItems = this.items.getClients(); 
    this.client = this.currentItems[0];
  }


  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addClient() {
    let addModal = this.modalCtrl.create(ClientCreatePage);
    addModal.onDidDismiss(item => {
      if (item) {
        console.log('inside addClient before calling the api')
        this.items.addClient(item);
      }
    })
    addModal.present();
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  clientSelected(item){
    //let client = {clientId: item.id, first_name: item.first_name};
    this.client = item;
    
  }

  filterClients(ev) {
    var val = ev.target.value;
    this.currentItems = this.items.getClients(val);
    return this.currentItems;
    
  }

  calculateAge(birthday) {
    if (birthday) {

      var timeDiff = Math.abs(Date.now() - Date.parse(birthday));
      //Used Math.floor instead of Math.ceil
      //so 26 years and 140 days would be considered as 26, not 27.
      return Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
    }
  }

}
