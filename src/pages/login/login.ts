import { Component } from '@angular/core';
import { NavController, ToastController,ModalController, AlertController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { HomePage } from '../../pages/pages';
import { User } from '../../providers/user';
import { SignupPage } from '../signup/signup';
import { UserData } from '../../models/user';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  responseData : any;
  userData: UserData;
  credentials: { username: string, password: string } = {
    username: '',
    password: ''
  };

  // Our translated text strings
  private loginErrorString: string;
  private signupErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController) {
    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  }

  doSignup() {
    let modal = this.modalCtrl.create(SignupPage);
    modal.onDidDismiss(item => {
      if (item) {
        this.user.signup(item).subscribe((resp) => {
          //let responseData = resp;
          //localStorage.setItem('userData', JSON.stringify(responseData.data.user))
          this.credentials.username = item.email;
          this.credentials.password = item.password;
          this.doLogin();
      // this.navCtrl.push(HomePage);
    }, (err) => {
      // Unable to sign up
      let toast = this.toastCtrl.create({
        message: this.signupErrorString,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
  }
    })
    modal.present();
  }


  // Attempt to login in through our User service
  doLogin() {
    if (this.credentials.username && this.credentials.password) {
      this.user.login(this.credentials).then((result) => {
        this.responseData = result;
        if (this.responseData.meta.status='ok') {
          localStorage.setItem('userData', JSON.stringify(this.responseData.data.user));
          localStorage.setItem('userId', this.credentials.username);
          localStorage.setItem('password', this.credentials.password);
          this.navCtrl.push(HomePage);
        }
        else {
          this.presentToast(this.loginErrorString);
        }
      }, (err) => {
        this.presentToast(this.loginErrorString);
      });
    }
    else {
      this.presentToast("Give username and password");
    }
  }

  forgotPassword(){
    let prompt = this.alertCtrl.create({
      title: 'Forgot Password?',
      message: "Please Enter your email linked with your account",
      inputs: [
        {
          name: 'email',
          placeholder: 'email@xyz.com'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            this.user.forgotPassword(data.email);
          }
        }
      ]
    });
    prompt.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  
}

