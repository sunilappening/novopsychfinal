import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
// import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the ReportPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-report',
  templateUrl: 'report.html',
})
export class ReportPage {
  link: string = this.navParams.get('url');
  options = {
    url: this.link,
    withCredentials: true
  }
  
  
  constructor(
    // public sanitizer: DomSanitizer,
    public navCtrl: NavController, public navParams: NavParams, 
    public modalCtrl: ModalController,public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportPage');
  }

  closeModal() {
    this.viewCtrl.dismiss();
}

}
