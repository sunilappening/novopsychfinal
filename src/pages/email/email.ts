import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { Assessment } from '../../models/assessment';
import { Items } from '../../providers/providers';

/**
 * Generated class for the EmailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-email',
  templateUrl: 'email.html',
})
export class EmailPage {
  email: string = "Select Assessment";
  clientId: string = this.navParams.get('clientId');
  clientName: string = this.navParams.get('clientName');
  assessments: Assessment[];
  assessmentsTag: Assessment[] = [];
  assessmentNameTag = [];
  assessment: Assessment;

  selectedAssessments: { short_name: string, extended_name: string } = {
    short_name: 'First Name',
    extended_name: 'Last Name'
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public items: Items) {
  }

  ionViewDidEnter() {
    console.log('value of clientId passed' + this.clientId);
    this.assessments = this.items.getAssessments();
    //this.assessment = this.assessments[0];
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  assessmentSelected(item) {
    var index = this.assessmentsTag.indexOf(item);
    if (index > -1) {
      item.selected = false;
  } else {
      item.selected = true;
  }
    //little hack to display the tag name
    this.assessmentsTag.push(item);
    this.assessmentNameTag.push(item.short_name);
  }

  onChange(val) {
    this.assessmentsTag.pop();
    this.assessmentNameTag.pop();
  }

  filterAssessments(ev) {
    var val = ev.target.value;
    this.assessments = this.items.getAssessments(val);
    return this.assessments;

  }


}
