import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssessmentTabPage } from './assessment-tab';

@NgModule({
  declarations: [
    AssessmentTabPage,
  ],
  imports: [
    IonicPageModule.forChild(AssessmentTabPage),
  ],
})
export class AssessmentTabPageModule {}
