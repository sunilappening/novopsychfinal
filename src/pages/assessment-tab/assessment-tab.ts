import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { Assessment } from '../../models/assessment';
import { Items } from '../../providers/providers';

/**
 * Generated class for the AssessmentTabPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-assessment-tab',
  templateUrl: 'assessment-tab.html',
})
export class AssessmentTabPage {

  clientId: string = this.navParams.get('clientId');
  assessments: Assessment[];
  assessmentsTag: Assessment[] = [];
  assessmentNameTag = [];
  assessment: Assessment;

  selectedAssessments: { short_name: string, extended_name: string} = {
    short_name: 'First Name',
    extended_name: 'Last Name'
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public items: Items) {
  }

  ionViewDidLoad() {
    console.log('value of clientId passed' + this.clientId);
    this.assessments = this.items.getAssessments();
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  assessmentSelected(item){
    //little hack to display the tag name
    this.assessmentsTag.push(item);
    this.assessmentNameTag.push(item.short_name);
    this.assessment = item;
  }

  onChange(val){
    this.assessmentsTag.pop();
    this.assessmentNameTag.pop();
  }

  filterAssessments(ev) {
    var val = ev.target.value;
    this.assessments = this.items.getAssessments(val);
    return this.assessments;
    
  }

}
