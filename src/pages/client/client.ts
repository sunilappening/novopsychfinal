import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController, Modal } from 'ionic-angular';

import { ClientCreatePage } from '../client-create/client-create';
import { SelectAssessmentPage } from '../select-assessment/select-assessment';
import { SchedulePage } from '../schedule/schedule';
import { EmailPage } from '../email/email';

import { Items } from '../../providers/providers';
import { Client } from '../../models/client';

/**
 * Generated class for the ClientPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-client',
  templateUrl: 'client.html',
})

export class ClientPage {
  currentItems: Client[];
  responseData: any;
  administer: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
  public modalCtrl: ModalController, public items: Items, public viewCtrl: ViewController) {
}

  //ionViewDidLoad() {
  ionViewDidEnter(){
    this.currentItems = this.items.getClients(); 
    this.administer = this.navParams.get('administer');
  }


  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addClient() {
    let addModal = this.modalCtrl.create(ClientCreatePage);
    addModal.onDidDismiss(item => {
      if (item) {
        this.items.addClient(item);
      }
    })
    addModal.present();
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  clientSelected(item){
    let client = {clientId: item.id, clientName: item.first_name + " " + item.last_name};
    let addModal: Modal;
    if(this.administer == 'administer'){
      addModal = this.modalCtrl.create(SelectAssessmentPage,client);
    }
    if(this.administer == 'schedule'){
      addModal = this.modalCtrl.create(SchedulePage,client);
    }
    if(this.administer == 'email'){
      addModal = this.modalCtrl.create(EmailPage,client);
    }
    
    addModal.present();
  }

  filterClients(ev) {
    var val = ev.target.value;
    this.currentItems = this.items.getClients(val);
    return this.currentItems;
    
  }

  calculateAge(birthday) {
    if (birthday) {

      var timeDiff = Math.abs(Date.now() - Date.parse(birthday));
      //Used Math.floor instead of Math.ceil
      //so 26 years and 140 days would be considered as 26, not 27.
      return Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
    }
  }

}
