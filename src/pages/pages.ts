import { MainPage } from './main-page/main-page';
import { AssessmentTabPage } from './assessment-tab/assessment-tab';
import { ClientTabPage } from './client-tab/client-tab';
import { AccountPage } from './account/account';
import { TabsPage } from './tabs/tabs';
import { LoginPage } from './login/login';

// The page the user lands on after opening the app and without a session
export const FirstRunPage = LoginPage;

// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const HomePage = TabsPage;

// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = MainPage;
export const Tab2Root = ClientTabPage;
export const Tab3Root = AssessmentTabPage;
export const Tab4Root = AccountPage;
