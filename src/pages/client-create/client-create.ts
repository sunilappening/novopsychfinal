import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavController, ViewController } from 'ionic-angular';


@Component({
  selector: 'page-client-create',
  templateUrl: 'client-create.html'
})
export class ClientCreatePage {
  
  client: { first_name: string, last_name: string, email: string, date_of_birth: string, sex_id: string} = {
    first_name: '',
    last_name: '',
    email: '',
    date_of_birth: '',
    sex_id:''
  };

  submitClient: boolean = false;
  isReadyToSave: boolean;

  item: any;

  form: FormGroup;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, formBuilder: FormBuilder) {
    let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    this.form = formBuilder.group({
      first_name: [this.client.first_name, Validators.required],
      last_name: [this.client.last_name, Validators.required],
      email: [this.client.email, Validators.compose([Validators.required, Validators.pattern(EMAIL_REGEXP)])],
      date_of_birth: [this.client.date_of_birth],
      sex_id: [this.client.sex_id]
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ionViewDidLoad() {

  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }
  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  done() {
    if (!this.form.valid) { return; }
    this.viewCtrl.dismiss(this.form.value);
  }
}
