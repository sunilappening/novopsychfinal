import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdministerPage } from './administer';

@NgModule({
  declarations: [
    AdministerPage,
  ],
  imports: [
    IonicPageModule.forChild(AdministerPage),
  ],
})
export class AdministerPageModule {}
